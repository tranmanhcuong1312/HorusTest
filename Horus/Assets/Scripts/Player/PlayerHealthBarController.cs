using UnityEngine.UI;
using UnityEngine;

public class PlayerHealthBarController : MonoBehaviour {

    private PlayerController player;

    [Header("Health")]
    [SerializeField] private Image health_bar;

    // Start is called before the first frame update
    void Start() {
        player = GameObject.Find("Player").transform.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update() {
        health_bar.fillAmount = (float) player.Health/player.Start_Health;
    }
}

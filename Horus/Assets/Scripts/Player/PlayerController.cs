using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    #region GameObject
    private Transform body;
    private Transform gun;
    private Transform bullet_point;
    [SerializeField] private Camera camera;
    [SerializeField] private LayerMask ground;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject canvas;
    #endregion

    #region Component
    #endregion

    #region Spinning
    [Header("Spinning")]
    [SerializeField] private float spinning_speed;
    #endregion

    #region Fire
    [Header("Fire")]
    [SerializeField] private float cooldown_time;
    private float cooldown_time_counter;
    #endregion

    #region Health
    [Header("Health")]
    [SerializeField] private int health;
    public int Health { get { return health;}}
    private int start_health;
    public int Start_Health { get { return start_health;}}
    #endregion

    private void Start() {
        Time.timeScale = 1f;
        body = transform.Find("Body");
        gun = body.Find("Gun");
        bullet_point = body.Find("BulletPoint");
        cooldown_time_counter = cooldown_time;
        start_health = health;
    }

    private void Update() {
        if (cooldown_time_counter > 0f) {
            cooldown_time_counter -= Time.deltaTime;
        }
        RaycastHit raycast_hit = new RaycastHit();
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out raycast_hit, 99f, ground)) {
            Vector3 vector = new Vector3(raycast_hit.point.x, transform.position.y, raycast_hit.point.z);
            Quaternion rotation = Quaternion.LookRotation(vector - transform.position, Vector3.up);
            rotation *= Quaternion.Euler(0, -90, 0);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, spinning_speed);
        }
        if (Input.GetMouseButtonDown(0) && cooldown_time_counter <= 0f) {
            GameObject new_bullet = Instantiate(bullet, bullet_point.position, Quaternion.identity);
            new_bullet.GetComponent<BulletController>().Direction = (bullet_point.position - gun.position).normalized;
            cooldown_time_counter = cooldown_time;
        }
        if (health <= 0) {
            canvas.GetComponent<GameOverMenuController>().Pause();
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 8) {
            health -= collision.gameObject.GetComponent<BulletController>().Damage;
        }
    }
}

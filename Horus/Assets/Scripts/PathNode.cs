using UnityEngine;

public class PathNode {
    private int i;
    public int I { get { return i;}}
    private int j;
    public int J { get { return j;}}
    private int g;
    public int G { get { return g;} set { this.g = value;}}
    private int h;
    public int H { get { return h;} set { this.h = value;}}
    private int f;
    public int F { get { return f;} set { this.f = value;}}
    private Vector3 position;

    public PathNode(int i, int j, int g, int h, Vector3 position) {
        this.i = i;
        this.j = j;
        this.g = g;
        this.h = h;
        this.position = position;
        f = g + h;
    }

    public Vector3 GetPosition() {
        return position;
    }
}

using UnityEngine.UI;
using UnityEngine;


public class EnemyController : MonoBehaviour{

    #region Game Objects
    private Transform player;
    private Transform canvas;
    private Transform head;
    private Transform gun;
    private Transform bullet_point;
    private Transform body;
    private Transform health_bar;
    private EnemiesController enemies;
    [SerializeField] private LayerMask ground;
    [SerializeField] private GameObject bullet;
    private Camera camera;
    #endregion

    #region Component
    private BoxCollider collider;
    #endregion

    #region State
    private EnemyState state;
    private bool on_ground;
    private int starting_area;
    public int Starting_Area { get { return starting_area;} set { this.starting_area = value;}}
    #endregion

    #region Ground Check
    [Header("Ground Check")]
    [SerializeField] private Transform ground_check_point;
    [SerializeField] private float ground_check_radius;
    #endregion

    #region Movement
    [Header("Movement")]
    [SerializeField] private float speed;
    private Vector3 target_position;
    private Vector3 moving_direction;
    #endregion

    #region Spinning
    [Header("Spinning")]
    [SerializeField] private float body_spinning_speed;
    [SerializeField] private float head_spinning_speed;
    private Vector3 gun_direction;
    #endregion

    #region Health
    [Header("Health")]
    [SerializeField] private int health;
    private Image image_health_bar;
    private int start_health;
    #endregion

    private void Start() {
        camera = Camera.main;
        player = GameObject.Find("Player").transform;
        canvas = GameObject.Find("Canvas").transform;
        enemies = GameObject.Find("Enemies").transform.GetComponent<EnemiesController>();
        body = transform.Find("Body");
        head = transform.Find("Head");
        health_bar = transform.Find("HealthBar");
        image_health_bar = health_bar.Find("Green").GetComponent<Image>();
        gun = head.Find("Gun");
        bullet_point = head.Find("BulletPoint");
        collider = GetComponent<BoxCollider>();
        state = EnemyState.Moving;
        target_position = Vector3.zero;
        gun_direction = gun.position - head.position;
        start_health = health;
        health_bar.rotation = Quaternion.LookRotation(transform.position - camera.transform.position);
    }

    private void Update() {
        StateUpdate();
        if (on_ground) {
            switch(state) {
                case EnemyState.MovingIntoArea:
                    break;
                case EnemyState.Moving:
                    if (target_position == Vector3.zero) {
                        target_position = enemies.AddEnemyPosition(transform.position);
                        moving_direction = target_position - transform.position;
                    }
                    break;
                case EnemyState.Spinning:
                    Quaternion to_rotation = Quaternion.LookRotation(player.position - head.position, Vector3.up);
                    to_rotation *= Quaternion.Euler(0, -90, 0);
                    head.rotation = Quaternion.RotateTowards(head.rotation, to_rotation, head_spinning_speed * Time.deltaTime);
                    if (head.rotation == to_rotation) {
                        state = EnemyState.Firing;
                    }
                    break;
                case EnemyState.SpinningBack:
                    Quaternion rotation = Quaternion.LookRotation(gun_direction, Vector3.up);
                    rotation *= Quaternion.Euler(0, -90, 0);
                    head.rotation = Quaternion.RotateTowards(head.rotation, rotation, head_spinning_speed * Time.deltaTime);
                    if (head.rotation == rotation) {
                        state = EnemyState.Moving;
                    }
                    break;
                case EnemyState.Firing:
                    GameObject new_bullet = Instantiate(bullet, bullet_point.position, Quaternion.identity);
                    new_bullet.GetComponent<BulletController>().Direction = (player.position - bullet_point.position).normalized;
                    enemies.RemoveEnemyPosition(target_position);
                    target_position = Vector3.zero;
                    state = EnemyState.SpinningBack;
                    break;
            }
        }

        if (health <= 0) {
            canvas.GetComponent<GameOverMenuController>().AddScore();
            Destroy(gameObject);
        }
    }

    private void StateUpdate() {
        on_ground = Physics.CheckSphere(ground_check_point.position, ground_check_radius, ground);
    }

    private void FixedUpdate() {
        if (target_position != Vector3.zero && state == EnemyState.Moving) {
            if (Mathf.Round(transform.position.x * 100)/100 != target_position.x || Mathf.Round(transform.position.z*100)/100 != target_position.z) {
                transform.position = Vector3.MoveTowards(transform.position, target_position, speed * Time.fixedDeltaTime);
            } else {
                state = EnemyState.Spinning;
            }
        } 
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 8) {
            health -= collision.gameObject.GetComponent<BulletController>().Damage;
            image_health_bar.fillAmount = (float) health/start_health;
        }
    }
}

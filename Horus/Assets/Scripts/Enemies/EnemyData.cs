using System;
using UnityEditor;

public enum EnemyState {
    MovingIntoArea, Moving, Waiting, Spinning, SpinningBack, Firing
}

[Serializable]
public class AreaProperties {
    public float x1;
    public float x2;
    public float z1;
    public float z2;
}
using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour {

    #region Moving area
    [Header("Moving Area")]
    [SerializeField] private Transform player;
    [SerializeField] private Transform moving_area;
    [SerializeField] private float forbidden_distance;
    public float Forbidden_Distance { get { return forbidden_distance;}}
    [SerializeField] private AreaProperties area_properties;
    private List<Vector3> enemy_positions = new List<Vector3>();
    #endregion
    
    private void Awake() {
        
    }

    private void Start() {
        
    }

    public Vector3 AddEnemyPosition(Vector3 position) {
        Vector3 vector = Vector3.zero;
        while (vector == Vector3.zero || enemy_positions.Contains(vector) || (vector.x < forbidden_distance && vector.z < forbidden_distance)) {
            vector = new Vector3(
                            (int) UnityEngine.Random.RandomRange(area_properties.x2, area_properties.x1 + 1),
                            position.y,
                            (int) UnityEngine.Random.RandomRange(area_properties.z2, area_properties.z1 + 1)
                            );
        }
        enemy_positions.Add(vector);
        return vector;
    }

    private float distance(Vector3 vector) {
        float a = Math.Abs(vector.x - player.position.x);
        float b = Math.Abs(vector.z - player.position.z);
        return (float) Math.Sqrt(a * a + b * b);
    }

    public void RemoveEnemyPosition(Vector3 vector3) {
        enemy_positions.Remove(vector3);
    }
}

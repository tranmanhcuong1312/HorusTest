using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour {

    #region Spawn
    [Header("Spawn")]
    [SerializeField] private GameObject enemy;
    [SerializeField] private List<Transform> spawn_areas;
    private List<AreaProperties> spawn_area_properties; 
    [SerializeField] private float spawn_time;
    [SerializeField] private float min_spawn_time;
    [SerializeField] private float decrease_spawn_time;
    private float spawn_time_counter;
    #endregion

    private void Start() {
        spawn_area_properties = new List<AreaProperties>();
        for (int i = 0 ; i < spawn_areas.Count; i++) {
            AreaProperties spawn = new AreaProperties();
            spawn.x1 = spawn_areas[i].position.x + spawn_areas[i].localScale.x/2;
            spawn.x2 = spawn_areas[i].position.x - spawn_areas[i].localScale.x/2;
            spawn.z1 = spawn_areas[i].position.z + spawn_areas[i].localScale.z/2;
            spawn.z2 = spawn_areas[i].position.z - spawn_areas[i].localScale.z/2;
            spawn_area_properties.Add(spawn);
        }
    }

    private void Update() {
        if (spawn_time_counter <= 0f) {
            Spawn();
            if (spawn_time > min_spawn_time) {
                spawn_time -= decrease_spawn_time;
            }
            spawn_time_counter = spawn_time;
        } else {
            spawn_time_counter -= Time.deltaTime;
        }
    }

    private void Spawn() {
        int area = Random.Range(0, spawn_areas.Count);
        Vector3 spawn_position = new Vector3(
                                    Random.RandomRange(spawn_area_properties[area].x2, spawn_area_properties[area].x1),
                                    0.55f,
                                    Random.RandomRange(spawn_area_properties[area].z2, spawn_area_properties[area].z1 )
                                );
        GameObject new_enemy = Instantiate(enemy, spawn_position, Quaternion.identity);
        new_enemy.GetComponent<EnemyController>().Starting_Area = area;
    }
}

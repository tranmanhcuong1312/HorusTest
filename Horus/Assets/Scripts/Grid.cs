using System;
using UnityEngine;

[Serializable]
public class Grid {

    public int start_width_point;
    public int start_height_point;
    public int width;
    public int height;
    public float cell_size;
    private PathNode[,] grid_array;
    public PathNode[,] Grid_Array { get { return grid_array;}}
    private TextMesh[,] text_array;

    public Grid(int start_width_point, int start_height_point, int width, int height, float cell_size) {
        this.start_width_point = start_width_point;
        this.start_height_point = start_height_point;
        this.width = width;
        this.height = height;
        this.cell_size = cell_size;
        grid_array = new PathNode[width, height];
        text_array = new TextMesh[width, height];
        for (int i = 0; i < grid_array.GetLength(0); i++) {
            for (int j = 0; j < grid_array.GetLength(1); j++) {
                grid_array[i, i] = new PathNode(i, j, 0, 0, new Vector3(i + start_width_point, 0f, j + start_height_point) + new Vector3(cell_size, 0, cell_size) * 0.5f);
                text_array[i, j] = CreateWorldText("0", Color.white, TextAnchor.MiddleCenter, null, new Vector3(i + start_width_point, 0f, j + start_height_point) + new Vector3(cell_size, 0, cell_size) * 0.5f, 5, TextAlignment.Center);
                Debug.DrawLine(new Vector3(i + start_width_point, 0f, j + start_height_point), 
                                new Vector3(i + start_width_point, 0f, j + start_height_point + 1f),
                                Color.white, 100f);
                Debug.DrawLine(new Vector3(i + start_width_point, 0f, j + start_height_point), 
                                new Vector3(i + start_width_point + 1f, 0f, j + start_height_point),
                                Color.white, 100f);
            }
        }
        Debug.DrawLine(
            new Vector3(0f + start_width_point, 0f, height + start_height_point), 
            new Vector3(width + start_width_point, 0f, height + start_height_point), 
            Color.white, 100f
        );
        Debug.DrawLine(
            new Vector3(width + start_width_point, 0f, 0f + start_height_point), 
            new Vector3(width + start_width_point, 0f, height + start_height_point), 
            Color.white, 100f
        );
    }

    private TextMesh CreateWorldText(string text, Color color, TextAnchor text_anchor, Transform parent = null, Vector3 location = default(Vector3), int font_size = 40, TextAlignment text_alignment = TextAlignment.Center) {
        GameObject game_object = new GameObject("World_Text", typeof(TextMesh));
        Transform transform = game_object.transform;
        transform.RotateAround(new Vector3(-1f, 0f, 0f), 90f);
        transform.SetParent(parent, false);
        transform.localPosition = location;
        TextMesh text_mesh = game_object.GetComponent<TextMesh>();
        text_mesh.anchor = text_anchor;
        text_mesh.alignment = text_alignment;
        text_mesh.text = text;
        text_mesh.fontSize = font_size;
        text_mesh.color = color;
        return text_mesh;
    }
}

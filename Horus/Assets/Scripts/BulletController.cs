using UnityEngine;

public class BulletController : MonoBehaviour {

    private Transform player;

    #region Component
    private Rigidbody rigidbody;
    #endregion

    #region Movement
    [Header("Movement")]
    [SerializeField] private float bullet_speed;
    private Vector3 direction;
    public Vector3 Direction { set { this.direction = value;}}
    #endregion

    #region Lifetime
    [Header("Lifetime")]
    [SerializeField] private float life_time;
    private float life_time_counter;
    #endregion

    #region Damage
    private int damage;
    public int Damage { get{ return damage;}}
    #endregion

    // Start is called before the first frame update
    void Start() {
        rigidbody = GetComponent<Rigidbody>();
        player = GameObject.Find("Player").transform;
        life_time_counter = life_time;
        damage = Random.RandomRange(1, 6);
    }

    // Update is called once per frame
    void Update() {
        life_time_counter -= Time.deltaTime;
        if (life_time_counter <= 0f) {
            Destroy(gameObject);
        }
        if (direction != null) {
            rigidbody.velocity = direction * bullet_speed;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 9) {
            Destroy(gameObject);
        }
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverMenuController : MonoBehaviour {

    public static bool paused = false;
    public static int score = 0;

    [SerializeField] private GameObject gameover_UI;
    [SerializeField] private Text score_text;

    private void Start() {
        gameover_UI.SetActive(false);
    }

    private void Update() {
        score_text.text = score.ToString();
    }

    public void AddScore() {
        score++;
    }

    public void Pause() {
        gameover_UI.SetActive(true);
        Time.timeScale = 0f;
        paused = true;
    }

    public void Replay() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

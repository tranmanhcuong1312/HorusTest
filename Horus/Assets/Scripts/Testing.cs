using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

// A* agorithm Testing
public class Testing : MonoBehaviour {
    [SerializeField] private Camera camera;
    [SerializeField] private int start_width_point;
    [SerializeField] private int start_height_point;
    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] private float cell_size;
    [SerializeField] private float forbidden_radius;

    private Grid grid;
    private List<PathNode> close = new List<PathNode>();
    private List<PathNode> open = new List<PathNode>();
    private List<PathNode> forbidden = new List<PathNode>();
    private PathNode start;
    private PathNode end;

    private void Start() {
        grid = new Grid(start_width_point, start_height_point, width, height, cell_size);
        start = grid.Grid_Array[0,0];
        end = grid.Grid_Array[19, 19];
        close.Add(start);
        AsteriskAlgorithm(start);
        Debug.Log(close);
    }

    private void AddForbiddenList() {

    }

    private void AsteriskAlgorithm(PathNode node) {
        if ((node.I == 0 || node.I == width -1) && (node.J == 0 || node.J == height - 1)) {
            AddCloseOfCornerPathNode(node);
        } else if (node.I == 0 || node.I == width -1 || node.J == 0 || node.J == height - 1) {
            AddCloseOfSidePathNode(node);
        } else {
            AddCloseOfMiddlePathNode(node);
        }
        PathNode min = CalculateValue();
        if (min != end) {
            AsteriskAlgorithm(min);
        }
    }
    
    private PathNode CalculateValue() {
        PathNode min = open[0];
        for (int i = 0; i < open.Count; i++) {
            open[i].G = 1;
            open[i].H = (int) (Math.Abs(end.GetPosition().x - open[i].GetPosition().x) +  Math.Abs(end.GetPosition().z - open[i].GetPosition().z));
            open[i].F = open[i].G + open[i].H;
            if (open[i].F < min.F) {
                min = open[i];
            }
        }
        return min;
    }

    private void AddCloseOfMiddlePathNode(PathNode node) {
        for (int n = node.I - 1; n < node.I + 2; n++) {
            for (int m = node.J - 1; m < node.J + 2; m++) {
                if (grid.Grid_Array[n, m] != node && !close.Contains(grid.Grid_Array[n, m]) && !forbidden.Contains(grid.Grid_Array[n, m])) {
                    open.Add(grid.Grid_Array[n, m]);
                }
            }
        }
    }

    private void AddCloseOfSidePathNode(PathNode node) {
        if (node.I == 0) {
            for (int n = node.I; n < node.I + 2; n++) {
                for (int m = node.J - 1; m < node.J + 2; m++) {
                    if (grid.Grid_Array[n, m] != node && !close.Contains(grid.Grid_Array[n, m]) && !forbidden.Contains(grid.Grid_Array[n, m])) {
                        open.Add(grid.Grid_Array[n, m]);
                    }
                }
            }
        } else if (node.I == width - 1) {
            for (int n = node.I - 1; n < node.I + 1; n++) {
                for (int m = node.J - 1; m < node.J + 2; m++) {
                    if (grid.Grid_Array[n, m] != node && !close.Contains(grid.Grid_Array[n, m]) && !forbidden.Contains(grid.Grid_Array[n, m])) {
                        open.Add(grid.Grid_Array[n, m]);
                    }
                }
            }
        } else if (node.J == 0) {
            for (int n = node.I - 1; n < node.I + 2; n++) {
                for (int m = node.J; m < node.J + 2; m++) {
                    if (grid.Grid_Array[n, m] != node && !close.Contains(grid.Grid_Array[n, m]) && !forbidden.Contains(grid.Grid_Array[n, m])) {
                        open.Add(grid.Grid_Array[n, m]);
                    }
                }
            }
        } else if (node.J == height - 1) {
            for (int n = node.I - 1; n < node.I + 2; n++) {
                for (int m = node.J - 1; m < node.J + 1; m++) {
                    if (grid.Grid_Array[n, m] != node && !close.Contains(grid.Grid_Array[n, m]) && !forbidden.Contains(grid.Grid_Array[n, m])) {
                        open.Add(grid.Grid_Array[n, m]);
                    }
                }
            }
        }
    }

    private void AddCloseOfCornerPathNode(PathNode node) {
        if (node.I == 0 && node.J == 0) {
            if (!close.Contains(grid.Grid_Array[1, 0])) open.Add(grid.Grid_Array[1, 0]);
            if (!close.Contains(grid.Grid_Array[0, 1])) open.Add(grid.Grid_Array[0, 1]);
            if (!close.Contains(grid.Grid_Array[1, 1])) open.Add(grid.Grid_Array[1, 1]);
        } else if (node.I == width - 1 && node.J == height - 1) {
            if (!close.Contains(grid.Grid_Array[width - 2, height -1])) open.Add(grid.Grid_Array[width - 2, height - 1]);
            if (!close.Contains(grid.Grid_Array[width - 1, height - 2])) open.Add(grid.Grid_Array[width - 1, height - 2]);
            if (!close.Contains(grid.Grid_Array[width - 2, height - 2])) open.Add(grid.Grid_Array[width - 2, height - 2]);
        } else if (node.I == 0 && node.J == height - 1) {
            if (!close.Contains(grid.Grid_Array[1, height - 1])) open.Add(grid.Grid_Array[1, height - 1]);
            if (!close.Contains(grid.Grid_Array[0, height - 2])) open.Add(grid.Grid_Array[0, height - 2]);
            if (!close.Contains(grid.Grid_Array[1, height - 2])) open.Add(grid.Grid_Array[1, height - 2]);
        } else if (node.I == width - 1 && node.J == 0) {
            if (!close.Contains(grid.Grid_Array[width - 2, 0])) open.Add(grid.Grid_Array[width - 2, 0]);
            if (!close.Contains(grid.Grid_Array[width - 1, 1])) open.Add(grid.Grid_Array[width - 1, 1]);
            if (!close.Contains(grid.Grid_Array[width - 2, 1])) open.Add(grid.Grid_Array[width - 2, 1]);
        } 
    }

    private void Update() {
        // if (Input.GetMouseButtonDown(0)) {
        //     Vector3 vector = camera.ScreenToWorldPoint(Input.mousePosition);
        //     vector.y = 0.55f;

        // }
    }
}
